/* eslint-env mocha */
'use strict'

const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}

const { expect } = require('chai')
const extractFromPreamble = require('../lib/extract-from-preamble').extractFromPreamble

const DEBUG = false

const doc = (level, contentContext, ...blocks) => {
  const doc = { context: 'document', contentContext, level, blocks }
  doc.document = doc
  return doc
}
const preamble = (...blocks) => {
  return { context: 'preamble', level: 0, blocks }
}
const sec = (level, ...blocks) => {
  return { context: 'section', level, blocks }
}
const par = (...blocks) => {
  return { context: 'paragraph', level: 0, blocks }
}

// const docPlain = (level, ...blocks) => doc(level, false, ...blocks)
// const docWpar = (level, ...blocks) => doc(level, false, par(), ...blocks)
// const docWsec = (level, ...blocks) => doc(level, false, sec(level + 1), ...blocks)
// const docWsecWpar = (level, ...blocks) => doc(level, false, secWpar(level), ...blocks)

const secWpar = (level, ...blocks) => sec(level, par(), ...blocks)

//This one can result in some unlikely structures, but the alternative is not a reasonable document structure.
const innerdocWpar = (level, ...blocks) => doc(level, 'paragaph', par(...blocks))
const innerdocWsec = (level, ...blocks) => doc(level, 'section', sec(level, ...blocks))
const innerdocWsecWpar = (level, ...blocks) => doc(level, 'section', secWpar(level, ...blocks))

const label = (node, skip = 0, parent = null, id = 0) => {
  node.$$id = ++id
  if (id === skip) node.$$id = ++id
  parent && (node.parent = parent)
  node.blocks.forEach((child) => {
    id = label(child, skip, node, id)
  })
  return id
}

const checkInOrder = (node, status, skip) => {
  status.index++
  if (skip === status.index) status.index++
  if (node.$$id !== status.index) {
    console.log(`node ${node.$$id} out of order, expected ${status.index}`)
    status.valid = false
  }
  node.blocks.forEach((child) => checkInOrder(child, status, skip))
}

// const replacer = (key, value) => key === 'parent' ? value.$$id : key === 'document' ? value.$$id : value

describe('extractFromPreamble tests', () => {
  function test (test, rp, deleted = 0, skip = 0) {
    const testCount = label(test)
    DEBUG && console.log('original', toString(test))
    const rpCount = label(rp, skip)
    expect(rpCount).to.equal(testCount)
    extractFromPreamble(test)
    DEBUG && console.log('reparented', toString(test))
    const status = { valid: true, index: 0 }
    checkInOrder(test, status, skip)
    expect(status.valid).to.equal(true)
    expect(toString(test)).to.equal(toString(rp))
    expect(test).to.deep.equal(rp)
  }

  it('actual preamble', () => {
    test(
      doc(0, false,
        preamble(par(),
          par()),
        sec(1)
      ),
      doc(0, false,
        preamble(par(),
          par()),
        sec(1)
      )
    )
  })

  it('preamble with doc with par', () => {
    test(
      doc(0, false,
        preamble(innerdocWpar(1),
          innerdocWpar(1)),
        sec(1)
      ),
      doc(0, false,
        preamble(innerdocWpar(1),
          innerdocWpar(1)),
        sec(1)
      )
    )
  })

  it('preamble with doc with doc with par', () => {
    test(
      doc(0, false,
        preamble(doc(0, 'paragraph', innerdocWpar(1)),
          innerdocWpar(0)),
        sec(1)
      ),
      doc(0, false,
        preamble(doc(0, 'paragraph', innerdocWpar(1)),
          innerdocWpar(0)),
        sec(1)
      )
    )
  })

  it('preamble with doc with section', () => {
    test(
      doc(0, false,
        preamble(innerdocWsec(1),
          innerdocWsecWpar(1)),
        sec(1)
      ),
      doc(0, false,
        innerdocWsec(1),
        innerdocWsecWpar(1),
        sec(1)
      ),
      1, 2)
  })

  it('preamble with doc with doc with section', () => {
    test(
      doc(0, false,
        preamble(doc(1, 'section', innerdocWsec(1)),
          innerdocWsecWpar(1)),
        sec(1)
      ),
      doc(0, false,
        doc(1, 'section', innerdocWsec(1)),
        innerdocWsecWpar(1),
        sec(1)
      ),
      1, 2)
  })
})
