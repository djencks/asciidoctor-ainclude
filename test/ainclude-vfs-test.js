/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const ainclude = require('../lib/ainclude')
const $fileContext = ainclude.$fileContext
const VfsContext = require('../lib/vfs-context')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

const IDRX = require('../lib/antora-adapter').IDRX

function checkId (doc, docCount = 1, index = 0, docIndex = 0) {
  const scan = (doc) => doc.blocks.reduce((accum, block) => {
    const docs = block.findBy({ context: 'document' })
    return accum.concat(docs, ...docs.map((subdoc) => scan(subdoc)))
  }, []
  )

  const id = Object.keys(ainclude.included)[index]
  const embedded = scan(doc)
  expect(embedded.length).to.equal(docCount)
  expect(embedded[docIndex][$fileContext]).to.equal(id)
  return Object.values(ainclude.included)[index]
}

function checkIdOriginalId (doc, docCount = 1, index = 0, docIndex = 0) {
  checkId(doc, docCount, index, docIndex)
  return { idprefix: '' }
}

describe('ainclude tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
    Object.keys(ainclude.included).forEach((key) => delete ainclude.included[key])
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]

    function getKey (target) {
      return `${target.src.version}@${target.src.component}:${target.src.module}:${target.src.family}$${target.src.relative}`
    }

    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            files: [{ src: {} }],
            read: function (resourceId) {
              const contentCatalog = mockContentCatalog(seed)
              const target = contentCatalog.resolveResource(resourceId, this.files[0].src)
              if (!target) {
                console.error(`could not locate ${resourceId} in context ${getKey(this.files[0].src)}`)
                return
              }
              this.files.unshift(target)
              const idprefix = `_${target.src.version}_${target.src.component}_${target.src.module}_${target.src.family}_${target.src.relative}`
                .replace(IDRX, '_')
              const token = getKey(target)
              const current = { token, file: target }
              return {
                content: (target.src.contents && target.src.contents.toString()) || target.contents.toString(),
                current,
                targetInfo: { idprefix, token },
                attributeOverrides: Opal.hash2([], {}),
              }
            },
            pop: function () {
              this.files.shift()
            },
          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
      {
        vfsType: 'antora with vfsContext',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
          vfsContext: new VfsContext(getKey({ src: {} }), { src: {} }),
        },
      },
    ]
    return vfss
  }

  ;[
    {
      type: 'global',
      f: (text, config, opts = {}) => {
        ainclude.register(asciidoctor.Extensions, config)
        return asciidoctor.load(text, opts)
      },
    },
    {
      type: 'registry',
      f: (text, config, opts = {}) => {
        const registry = ainclude.register(asciidoctor.Extensions.create(), config)
        return asciidoctor.load(text, Object.assign(opts, { extension_registry: registry }))
      },
    },
  ].forEach(({ type, f }) => {
    prepareVfss([
      {
        version: '4.5',
        family: 'page',
        relative: 'page-a.adoc',
        contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'page-a-attr.adoc',
        contents: `This is a paragraph containing {count} sentences.
This is the second {type}.
This is the {kind} sentence.
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'page-section.adoc',
        contents: `== This is a section

With a paragraph with two sentences.
The second sentence.
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'page-section2.adoc',
        contents: `= This is a section

With a paragraph with two sentences.
The second sentence of section2.
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'page-section3.adoc',
        contents: `= This is a section3

With a paragraph with two sentences.
The second sentence of section3.

ainclude::page-section2.adoc[+1]
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'table.adoc',
        contents: `= A Table of Conceptual Importance

[options=header,separator=|]
|===
|Concept|Importance

|Content
|Utmost

|Understanding
|Primary

|Conceptuality
|Theoretical
|===
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'simple/first-child.adoc',
        contents: `= This is a first child page for the simple ainclude example

Here is some content, linking to the first section of the second page: xref:simple/second-child.adoc[].

== This is the second section of the first child page

Here is some content, linking to the second section of the second child page: xref:simple/second-child.adoc#_this_is_the_second_section_of_the_second_child_page[].

Here is a link to the custom content on the second page: xref:simple/second-child.adoc#_custom_id_2[].

Here is a link to the custom content on this page: xref:#_custom_id[]

'''
[[_custom_id,Custom Id]]
Here is a paragraph with a custom Id.
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'simple/second-child.adoc',
        contents: `= This is a second child page for the simple ainclude example

Here is some content, linking to the first section of the first page: xref:simple/first-child.adoc[].

== This is the second section of the second child page

Here is some content, linking to the second section of the first child page: xref:simple/first-child.adoc#_this_is_the_second_section_of_the_first_child_page[].

Here is a link to the custom content on the first page: xref:simple/first-child.adoc#_custom_id[].

Here is a link to the custom content on this page: xref:#_custom_id_2[]

'''
[[_custom_id_2,Custom Id]]
Here is a paragraph with a custom Id.
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'page-section-parent.adoc',
        contents: `== This is section 3

The first sentence references the included doc via xref:page-section.adoc[].
The second sentence references the second section of the included doc via xref:page-section.adoc#_this_is_section_2[].

== This is section 4

With a paragraph with two sentences.
The second sentence.

ainclude::page-section.adoc[]
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'whitelist-parent.adoc',
        contents: `= Whitelist parent
:docdate: 2020-7-1
:sectnums:
:sectnumlevels: 5

ainclude::whitelist-child.adoc[+1]
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'whitelist-child.adoc',
        contents: `= Whitelist child
docdate: {docdate}

sectnums: {sectnums}

sectnumlevels: {sectnumlevels}
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'dlist-child.adoc',
        contents: `= Description List child

[[item_one]]Item one:: [[description_one]]Description one
Item two:: Description two
[[subitem_one]]Subitem::: [[subdescription_one]]Subdescription
`,
      },
    ]).forEach(({ vfsType, config }) => {
      it(`include a paragraph, ${type}, ${vfsType}`, () => {
        const doc = f(`= Test Paragraph Inclusion

ainclude::4.5@component-a:module-a:page-a.adoc[]
`, config)
        expect(doc.attributes.$$smap.doctitle).to.equal('Test Paragraph Inclusion')
        expect(Object.keys(ainclude.included).length).to.equal(1)
        const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
        const html = doc.convert()
        expect(html).to.equal(`<div ${id}class="paragraph">
<p>This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.</p>
</div>`)
      })

      it(`include a paragraph, attributes in macro, ${type}, ${vfsType}`, () => {
        const doc = f(`= Test Attributes in Inclusion

ainclude::4.5@component-a:module-a:page-a-attr.adoc[+1,count='thousands of',type='utterance',kind='post-penultimate']
`, config)
        expect(doc.attributes.$$smap.doctitle).to.equal('Test Attributes in Inclusion')
        expect(Object.keys(ainclude.included).length).to.equal(1)
        const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
        const html = doc.convert()
        expect(html).to.equal(`<div ${id}class="paragraph">
<p>This is a paragraph containing thousands of sentences.
This is the second utterance.
This is the post-penultimate sentence.</p>
</div>`)
      })

      function whitelistTest (f, configAttr, blockAttr) {
        const doc = f(`= Whitelist parent
:docdate: 2020-7-1
:sectnums:
:sectnumlevels: 5

ainclude::4.5@component-a:module-a:whitelist-child.adoc[+1,${blockAttr}]
`, config, configAttr)
        expect(doc.attributes.$$smap.doctitle).to.equal('Whitelist parent')
        expect(Object.keys(ainclude.included).length).to.equal(1)
        const id = 'id="' + Object.values(ainclude.included)[0].docid + '"'
        const html = doc.convert()
        expect(html).to.equal(`<div class="sect1">
<h2 ${id}>1. Whitelist child</h2>
<div class="sectionbody">
<div class="paragraph">
<p>docdate: 2020-7-1</p>
</div>
<div class="paragraph">
<p>sectnums: </p>
</div>
<div class="paragraph">
<p>sectnumlevels: 5</p>
</div>
</div>
</div>`)
      }

      ;[
        {
          attrLoc: 'global',
          configAttr: { attributes: { 'ainclude-default-options': 'all_attributes' } },
          blockAttr: '',
        },
        {
          attrLoc: 'block',
          configAttr: {},
          blockAttr: 'all_attributes',
        },
      ].forEach(({ attrLoc, configAttr, blockAttr }) => {
        it(`include a paragraph, attributes in header and body, ${type}, ${vfsType}, ${attrLoc}`, () => {
          const doc = f(`= Test Attributes in Inclusion
:count: thousands of

:type: utterance

== This is a separating section

With content

:kind: post-penultimate

ainclude::4.5@component-a:module-a:page-a-attr.adoc[+1,${blockAttr}]
`, config, configAttr)
          expect(doc.attributes.$$smap.doctitle).to.equal('Test Attributes in Inclusion')
          expect(Object.keys(ainclude.included).length).to.equal(1)
          const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_separating_section">This is a separating section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With content</p>
</div>
<div ${id}class="paragraph">
<p>This is a paragraph containing thousands of sentences.
This is the second utterance.
This is the post-penultimate sentence.</p>
</div>
</div>
</div>`)
        })
        it(`whitelist, ${type}, ${vfsType}, ${attrLoc}`, () => {
          whitelistTest(f, configAttr, blockAttr)
        })
      })

      ;[
        {
          attrLoc: 'global',
          configAttr: { attributes: { 'ainclude-default-options': 'header_attributes' } },
          blockAttr: '',
        },
        {
          attrLoc: 'block',
          configAttr: {},
          blockAttr: 'header_attributes',
        },
      ].forEach(({ attrLoc, configAttr, blockAttr }) => {
        it(`include a paragraph, attributes in header and body, header_attributes, ${type}, ${vfsType}, ${attrLoc}`, () => {
          const doc = f(`= Test Attributes in Inclusion
:count: thousands of

:type: utterance

== This is a separating section

With content

:kind: post-penultimate

ainclude::4.5@component-a:module-a:page-a-attr.adoc[+1,${blockAttr}]
`, config, configAttr)
          expect(doc.attributes.$$smap.doctitle).to.equal('Test Attributes in Inclusion')
          expect(Object.keys(ainclude.included).length).to.equal(1)
          const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_separating_section">This is a separating section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With content</p>
</div>
<div ${id}class="paragraph">
<p>This is a paragraph containing thousands of sentences.
This is the second {type}.
This is the {kind} sentence.</p>
</div>
</div>
</div>`)
        })
        it(`whitelist, ${type}, ${vfsType}, ${attrLoc}`, () => {
          whitelistTest(f, configAttr, blockAttr)
        })
      })

      ;[
        {
          attrLoc: 'default',
          configAttr: {},
          blockAttr: '',
        },
        {
          attrLoc: 'global',
          configAttr: { attributes: { 'ainclude-default-options': 'attribute_overrides' } },
          blockAttr: '',
        },
        {
          attrLoc: 'block',
          configAttr: {},
          blockAttr: 'attribute_overrides',
        },
      ].forEach(({ attrLoc, configAttr, blockAttr }) => {
        it(`include a paragraph, attributes in header and body, attribute_overrides, ${type}, ${vfsType}, ${attrLoc}`, () => {
          const doc = f(`= Test Attributes in Inclusion
:count: thousands of

:type: utterance

== This is a separating section

With content

:kind: post-penultimate

ainclude::4.5@component-a:module-a:page-a-attr.adoc[+1,${blockAttr}]
`, config, configAttr)
          expect(doc.attributes.$$smap.doctitle).to.equal('Test Attributes in Inclusion')
          expect(Object.keys(ainclude.included).length).to.equal(1)
          const id = checkId(doc)
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_separating_section">This is a separating section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With content</p>
</div>
<div id="${id.docid}" class="paragraph">
<p>This is a paragraph containing {count} sentences.
This is the second {type}.
This is the {kind} sentence.</p>
</div>
</div>
</div>`)
        })
        it(`whitelist, ${type}, ${vfsType}, ${attrLoc}`, () => {
          whitelistTest(f, configAttr, blockAttr)
        })
      })

      it(`include a section, ${type}, ${vfsType}`, () => {
        const doc = f(`= Test Section Inclusion

ainclude::4.5@component-a:module-a:page-section.adoc[]
`, config)
        expect(Object.keys(ainclude.included).length).to.equal(1)
        const id = checkId(doc)
        expect(doc.attributes.$$smap.doctitle).to.equal('Test Section Inclusion')
        const html = doc.convert()
        expect(html).to.equal(`<div class="sect1">
<h2 id="${id.idprefix}_this_is_a_section">This is a section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
</div>
</div>`)
      })

      it(`include a section2, leveloffset works, ${type}, ${vfsType}`, () => {
        const doc = f(`= Test Section Inclusion

ainclude::4.5@component-a:module-a:page-section2.adoc[+1]
`, config)
        expect(Object.keys(ainclude.included).length).to.equal(1)
        const id = checkId(doc)
        expect(doc.attributes.$$smap.doctitle).to.equal('Test Section Inclusion')
        const html = doc.convert()
        expect(html).to.equal(`<div class="sect1">
<h2 id="${id.idprefix}_this_is_a_section">This is a section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section2.</p>
</div>
</div>
</div>`)
      })

      ;[
        {
          attrLoc: 'default',
          headerAttr: '',
          configAttr: {},
          blockAttr: '',
          checkId1: checkId,
          checkId2: checkId,
        },
        {
          attrLoc: 'config',
          headerAttr: '',
          configAttr: { attributes: { 'ainclude-default-options': 'original_ids' } },
          checkId1: checkIdOriginalId,
          checkId2: checkIdOriginalId,
        },
        {
          attrLoc: 'header',
          headerAttr: ':ainclude-default-options: original_ids',
          configAttr: {},
          blockAttr: '',
          checkId1: checkIdOriginalId,
          checkId2: checkId,
        },
        {
          attrLoc: 'block',
          headerAttr: '',
          configAttr: {},
          blockAttr: ',original_ids',
          checkId1: checkIdOriginalId,
          checkId2: checkId,
        },
      ].forEach(({ attrLoc, headerAttr, configAttr, blockAttr, checkId1, checkId2 }) => {
        it(`include a section3, nested, ${type}, ${vfsType}, ${attrLoc}`, () => {
          const doc = f(`= Test Nested Section Inclusion
${headerAttr}

ainclude::4.5@component-a:module-a:page-section3.adoc[+1${blockAttr}]
`, config, configAttr)
          expect(Object.keys(ainclude.included).length).to.equal(2)
          const id1 = checkId1(doc, 2, 1, 0)
          const id2 = checkId2(doc, 2, 0, 1)
          expect(doc.attributes.$$smap.doctitle).to.equal('Test Nested Section Inclusion')
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="${id1.idprefix}_this_is_a_section3">This is a section3</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section3.</p>
</div>
<div class="sect2">
<h3 id="${id2.idprefix}_this_is_a_section">This is a section</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section2.</p>
</div>
</div>
</div>
</div>`)
        })

        //(inline) ids on list items are not available to a tree processor, so these are always 'original_id' style.
        //See https://github.com/asciidoctor/asciidoctor/issues/2721
        it(`dlist with custom ids, ${type}, ${vfsType}, ${attrLoc}`, () => {
          const doc = f(`= Doc

ainclude::4.5@component-a:module-a:dlist-child.adoc[+1]
`, config)
          checkId(doc, 1, 0, 0)
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_dlist_child_adoc_description_list_child">Description List child</h2>
<div class="sectionbody">
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="item_one"></a>Item one</dt>
<dd>
<p><a id="description_one"></a>Description one</p>
</dd>
<dt class="hdlist1">Item two</dt>
<dd>
<p>Description two</p>
<div class="dlist">
<dl>
<dt class="hdlist1"><a id="subitem_one"></a>Subitem</dt>
<dd>
<p><a id="subdescription_one"></a>Subdescription</p>
</dd>
</dl>
</div>
</dd>
</dl>
</div>
</div>
</div>`)
        })
      })

      it(`include a table, ${type}, ${vfsType}`, () => {
        const doc = f(`= Test Table Inclusion

ainclude::4.5@component-a:module-a:table.adoc[+1]
`, config)
        const id = checkId(doc)
        expect(doc.attributes.$$smap.doctitle).to.equal('Test Table Inclusion')
        const html = doc.convert()
        expect(html).to.equal(`<div class="sect1">
<h2 id="${id.idprefix}_a_table_of_conceptual_importance">A Table of Conceptual Importance</h2>
<div class="sectionbody">
<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 50%;">
<col style="width: 50%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">Concept</th>
<th class="tableblock halign-left valign-top">Importance</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">Content</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Utmost</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">Understanding</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Primary</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">Conceptuality</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Theoretical</p></td>
</tr>
</tbody>
</table>
</div>
</div>`)
      })

      it(`simple topic test from antora-pdf, ${type}, ${vfsType}`, () => {
        //This test does not include an appropriate xref transformer,
        // so the hrefs are "wrong" and don't point to the assembled document
        const doc = f(`= Ainclude parent page

This is a simple example of \`ainclude\` functionality

ainclude::4.5@component-a:module-a:simple/first-child.adoc[+1]

ainclude::4.5@component-a:module-a:simple/second-child.adoc[+1]
`, config)
        checkId(doc, 2, 0, 0)
        checkId(doc, 2, 1, 1)
        expect(doc.attributes.$$smap.doctitle).to.equal('Ainclude parent page')
        const html = doc.convert()
        expect(html).to.equal(`<div class="paragraph">
<p>This is a simple example of <code>ainclude</code> functionality</p>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_simple_first_child_adoc_this_is_a_first_child_page_for_the_simple_ainclude_example">This is a first child page for the simple ainclude example</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Here is some content, linking to the first section of the second page: <a href="simple/second-child.html">simple/second-child.html</a>.</p>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_simple_first_child_adoc_this_is_the_second_section_of_the_first_child_page">This is the second section of the first child page</h3>
<div class="paragraph">
<p>Here is some content, linking to the second section of the second child page: <a href="simple/second-child.html#_this_is_the_second_section_of_the_second_child_page">simple/second-child.html</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on the second page: <a href="simple/second-child.html#_custom_id_2">simple/second-child.html</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on this page: <a href="#_custom_id">[_custom_id]</a></p>
</div>
<hr>
<div id="_4_5_component_a_module_a_page_simple_first_child_adoc_custom_id" class="paragraph">
<p>Here is a paragraph with a custom Id.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_simple_second_child_adoc_this_is_a_second_child_page_for_the_simple_ainclude_example">This is a second child page for the simple ainclude example</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Here is some content, linking to the first section of the first page: <a href="simple/first-child.html">simple/first-child.html</a>.</p>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_simple_second_child_adoc_this_is_the_second_section_of_the_second_child_page">This is the second section of the second child page</h3>
<div class="paragraph">
<p>Here is some content, linking to the second section of the first child page: <a href="simple/first-child.html#_this_is_the_second_section_of_the_first_child_page">simple/first-child.html</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on the first page: <a href="simple/first-child.html#_custom_id">simple/first-child.html</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on this page: <a href="#_custom_id_2">[_custom_id_2]</a></p>
</div>
<hr>
<div id="_4_5_component_a_module_a_page_simple_second_child_adoc_custom_id_2" class="paragraph">
<p>Here is a paragraph with a custom Id.</p>
</div>
</div>
</div>
</div>`)
      })

      it(`cross component/version ainclude, nested from antora-pdf, ${type}, ${vfsType}`, () => {
        const doc = f(`== This is a parent section

The first sentence references the included doc via xref:4.5@component-a:module-a:page-section.adoc[].
The second sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section.adoc#_this_is_section_2[].

The third sentence references the included doc via xref:4.5@component-a:module-a:page-section-parent.adoc[].
The fourth sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section-parent.adoc#_this_is_section_4[].

ainclude::4.5@component-a:module-a:page-section-parent.adoc[+1]

`, config)
        checkId(doc, 2, 0, 1)
        checkId(doc, 2, 1, 0)
        const html = doc.convert()
        expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_parent_section">This is a parent section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The first sentence references the included doc via <a href="4.5@component-a:module-a:page-section.html">4.5@component-a:module-a:page-section.html</a>.
The second sentence references the second section of the included doc via <a href="4.5@component-a:module-a:page-section.html#_this_is_section_2">4.5@component-a:module-a:page-section.html</a>.</p>
</div>
<div class="paragraph">
<p>The third sentence references the included doc via <a href="4.5@component-a:module-a:page-section-parent.html">4.5@component-a:module-a:page-section-parent.html</a>.
The fourth sentence references the second section of the included doc via <a href="4.5@component-a:module-a:page-section-parent.html#_this_is_section_4">4.5@component-a:module-a:page-section-parent.html</a>.</p>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section_parent_adoc_this_is_section_3">This is section 3</h3>
<div class="paragraph">
<p>The first sentence references the included doc via <a href="page-section.html">page-section.html</a>.
The second sentence references the second section of the included doc via <a href="page-section.html#_this_is_section_2">page-section.html</a>.</p>
</div>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section_parent_adoc_this_is_section_4">This is section 4</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section_adoc_this_is_a_section">This is a section</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
</div>
</div>
</div>`)
      })
    })
  })
})
