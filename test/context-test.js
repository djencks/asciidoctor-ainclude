/* eslint-env mocha */
'use strict'

//This tests the attributeOverrides provided by the Antora adapter,
// which mimics (due to copied code) the attribute overrides supplied by Antora.

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const ainclude = require('../lib/ainclude')
const VfsContext = require('../lib/vfs-context')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

describe('ainclude tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
    Object.keys(ainclude.included).forEach((key) => delete ainclude.included[key])
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]

    function getKey (target) {
      return `${target.src.version}@${target.src.component}:${target.src.module}:${target.src.family}$${target.src.relative}`
    }

    const vfss = [
      {
        vfsType: 'antora',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
      {
        vfsType: 'antora with vfsContext',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
          vfsContext: new VfsContext(getKey({ src: {} }), { src: {} }),
        },
      },
    ]
    return vfss
  }

  ;[
  //   {
  //   type: 'global',
  //   f: (text, config, opts = {}) => {
  //     ainclude.register(asciidoctor.Extensions, config)
  //     return asciidoctor.load(text, opts)
  //   },
  // },
    {
      type: 'registry',
      f: (text, config, opts = {}) => {
        const registry = ainclude.register(asciidoctor.Extensions.create(), config)
        return asciidoctor.load(text, Object.assign(opts, { extension_registry: registry }))
      },
    },
  ].forEach(({ type, f }) => {
    prepareVfss([
      {
        version: '4.5',
        family: 'page',
        relative: 'page-a.adoc',
        contents: `= Page A
      
docname: {docname}
page-component-name: {page-component-name}
page-module: {page-module}
page-relative: {page-relative}
`,
      },
      {
        version: '4.0',
        family: 'page',
        relative: 'page-b.adoc',
        contents: `= Page B
      
docname: {docname}
page-component-name: {page-component-name}
page-module: {page-module}
page-relative: {page-relative}
`,
      },
      {
        version: '4.5',
        family: 'page',
        module: 'module-c',
        relative: 'page-c.adoc',
        contents: `= Page C
      
docname: {docname}
page-component-name: {page-component-name}
page-module: {page-module}
page-relative: {page-relative}
`,
      },
      {
        version: '4.5',
        family: 'page',
        relative: 'topic/page-d.adoc',
        contents: `= Page D
      
docname: {docname}
page-component-name: {page-component-name}
page-module: {page-module}
page-relative: {page-relative}
`,
      },
    ]).forEach(({ vfsType, config }) => {
      it(`attribute overrides, ${type}, ${vfsType}`, () => {
        const doc = f(`= Test Attribute Overrides
:leveloffset: 1

ainclude::4.5@component-a:module-a:page-a.adoc[]
ainclude::4.0@component-a:module-a:page-b.adoc[]
ainclude::4.5@component-a:module-c:page-c.adoc[]
ainclude::4.5@component-a:module-a:topic/page-d.adoc[]
`, config)
        expect(doc.attributes.$$smap.doctitle).to.equal('Test Attribute Overrides')
        expect(Object.keys(ainclude.included).length).to.equal(4)
        const html = doc.convert()
        expect(html).to.equal(`<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_page_a_adoc_page_a">Page A</h2>
<div class="sectionbody">
<div class="paragraph">
<p>docname: page-a
page-component-name: component-a
page-module: module-a
page-relative: page-a.adoc</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_0_component_a_module_a_page_page_b_adoc_page_b">Page B</h2>
<div class="sectionbody">
<div class="paragraph">
<p>docname: page-b
page-component-name: component-a
page-module: module-a
page-relative: page-b.adoc</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_c_page_page_c_adoc_page_c">Page C</h2>
<div class="sectionbody">
<div class="paragraph">
<p>docname: page-c
page-component-name: component-a
page-module: module-c
page-relative: page-c.adoc</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_topic_page_d_adoc_page_d">Page D</h2>
<div class="sectionbody">
<div class="paragraph">
<p>docname: topic/page-d
page-component-name: component-a
page-module: module-a
page-relative: topic/page-d.adoc</p>
</div>
</div>
</div>`)
      })
    })
  })
})
