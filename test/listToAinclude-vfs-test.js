/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const ainclude = require('./../lib/ainclude')
const VfsContext = require('../lib/vfs-context')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

const ROOT_FILE = {
  src: {
    version: '4.5',
    component: 'component-a',
    module: 'module-a',
    family: 'page',
    relative: 'root.adoc',
  },
}

const IDRX = require('./../lib/antora-adapter').IDRX

describe('listToAinclude tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
    Object.keys(ainclude.included).forEach((key) => delete ainclude.included[key])
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const contentCatalog = mockContentCatalog(seed)

    function getKey (target) {
      return `${target.src.version}@${target.src.component}:${target.src.module}:${target.src.family}$${target.src.relative}`
    }

    const vfss = [
      {
        vfsType: 'plain',
        config: () => {
          return {
            vfs: {
              files: [ROOT_FILE],
              getKey: (target) => {
                return `${target.src.version}@${target.src.component}:${target.src.module}:${target.src.family}$${target.src.relative}`
              },
              read: function (resourceId) {
                const target = contentCatalog.resolveResource(resourceId, this.files[0].src)
                if (!target) {
                  console.warn(`Could not locate ${resourceId} in context of ${this.getKey(this.files[0])}`)
                  return
                }
                // console.log(`located ${resourceId} in context of ${this.getKey(this.files[0])}`)
                this.files.unshift(target)
                const idprefix = `_${target.src.version}_${target.src.component}_${target.src.module}_${target.src.family}_${target.src.relative}`
                  .replace(IDRX, '_')
                const token = getKey(target)
                const current = { token, file: target }
                return {
                  content: (target.src.contents && target.src.contents.toString()) || target.contents.toString(),
                  current,
                  targetInfo: { idprefix, token },
                  attributeOverrides: Opal.hash2([], {}),
                }
              },
              pop: function () { this.files.shift() },
            },
          }
        },
      },
      {
        vfsType: 'antora',
        config: () => {
          return {
            file: ROOT_FILE,
            contentCatalog: mockContentCatalog(seed),
            config: { attributes: {} },
          }
        },
      },
      {
        vfsType: 'antora with vfsContext',
        config: () => {
          return {
            file: ROOT_FILE,
            contentCatalog: mockContentCatalog(seed),
            config: { attributes: {} },
            vfsContext: new VfsContext(getKey(ROOT_FILE), ROOT_FILE),
          }
        },
      },
    ]
    return vfss
  }

  ;[
  //   {
  //   type: 'global',
  //   f: (text, config, opts = {}) => {
  //     ainclude.register(asciidoctor.Extensions, config)
  //     return asciidoctor.load(text, opts)
  //   },
  // },
    {
      type: 'registry',
      f: (text, config, opts = {}) => {
        const registry = ainclude.register(asciidoctor.Extensions.create(), config)
        return asciidoctor.load(text, Object.assign(opts, { extension_registry: registry }))
      },
    }].forEach(({ type, f }) => {
    prepareVfss([{
      version: '4.5',
      family: 'page',
      relative: 'page-a.adoc',
      contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-a-attr.adoc',
      contents: `This is a paragraph containing {count} sentences.
This is the second {type}.
This is the {kind} sentence.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section.adoc',
      contents: `= This is a section

With a paragraph with two sentences.
The second sentence.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section2.adoc',
      contents: `= This is a section 2

With a paragraph with two sentences.
The second sentence of section two.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section3.adoc',
      contents: `= This is a section 3

With a paragraph with two sentences.
The second sentence of section three.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'table.adoc',
      contents: `= A Table of Conceptual Importance

[options=header,separator=|]
|===
|Concept|Importance

|Content
|Utmost

|Understanding
|Primary

|Conceptuality
|Theoretical
|===
`,
    },
    {
      version: '4.5',
      family: 'nav',
      relative: 'nav1.adoc',
      contents: `
* xref:page-section.adoc[]
* xref:page-section2.adoc[]
** xref:page-section3.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'nav',
      relative: 'nav2.adoc',
      contents: `
* The Start of the List
* xref:page-section.adoc[]
** xref:page-section2.adoc[]
* A second Top Item
** xref:page-section3.adoc[]
`,
    },
    ]).forEach(({ vfsType, config }) => {
      ;[
        { sectnums: ':ainclude-default-options: header_attributes\n:sectnums:', sectnumf: (s) => s, sectnumType: 'sectnums' },
        { sectnums: '', sectnumf: (s) => '', sectnumType: 'no sectnums' },
      ].forEach(({ sectnums, sectnumf, sectnumType }) => {
        it(`listToAinclude, ${type}, ${vfsType}, ${sectnumType}`, () => {
          const doc = f(`= Test nav list inclusion
${sectnums}

== This is a section before

With a short paragraph.

=== This is a subsection before

With a short paragraph.

listToAinclude::4.5@component-a:module-a:nav$nav1.adoc[]

== This is a section after.

With a short paragraph.
`, config())
          expect(doc.attributes.$$smap.doctitle).to.equal('Test nav list inclusion')
          expect(Object.keys(ainclude.included).length).to.equal(3)
          structure(doc)
          // const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_section_before">${sectnumf('1. ')}This is a section before</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
<div class="sect2">
<h3 id="_this_is_a_subsection_before">${sectnumf('1.1. ')}This is a subsection before</h3>
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_page_section_adoc_this_is_a_section">${sectnumf('2. ')}This is a section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_page_section2_adoc_this_is_a_section_2">${sectnumf('3. ')}This is a section 2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section two.</p>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section3_adoc_this_is_a_section_3">${sectnumf('3.1. ')}This is a section 3</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section three.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_this_is_a_section_after">${sectnumf('4. ')}This is a section after.</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
</div>
</div>`)
        })

        it(`listToAinclude with text list items, ${type}, ${vfsType}, ${sectnumType}`, () => {
          const doc = f(`= Test nav list inclusion
${sectnums}

== This is a section before

With a short paragraph.

=== This is a subsection before

With a short paragraph.

listToAinclude::4.5@component-a:module-a:nav$nav2.adoc[]

== This is a section after.

With a short paragraph.
`, config())
          expect(doc.attributes.$$smap.doctitle).to.equal('Test nav list inclusion')
          expect(Object.keys(ainclude.included).length).to.equal(3)
          structure(doc)
          // const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_section_before">${sectnumf('1. ')}This is a section before</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
<div class="sect2">
<h3 id="_this_is_a_subsection_before">${sectnumf('1.1. ')}This is a subsection before</h3>
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_the_start_of_the_list">${sectnumf('2. ')}The Start of the List</h2>
<div class="sectionbody">

</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_page_section_adoc_this_is_a_section">${sectnumf('3. ')}This is a section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section2_adoc_this_is_a_section_2">${sectnumf('3.1. ')}This is a section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section two.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_a_second_top_item">${sectnumf('4. ')}A second Top Item</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section3_adoc_this_is_a_section_3">${sectnumf('4.1. ')}This is a section 3</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section three.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_this_is_a_section_after">${sectnumf('5. ')}This is a section after.</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
</div>
</div>`)
        })

        it(`plain ainclude, ${type}, ${vfsType}, ${sectnumType}`, () => {
          const doc = f(`= Test nav list inclusion
${sectnums}

== This is a section before

With a short paragraph.

=== This is a subsection before

With a short paragraph.

ainclude::page-section.adoc[+1]
ainclude::page-section2.adoc[+1]
ainclude::page-section3.adoc[+2]
// listToAinclude::4.5@component-a:module-a:nav$nav1.adoc[]

== This is a section after.

With a short paragraph.
`, config())
          expect(doc.attributes.$$smap.doctitle).to.equal('Test nav list inclusion')
          expect(Object.keys(ainclude.included).length).to.equal(3)
          structure(doc)
          // const id = 'id="' + Object.values(ainclude.included)[0].docid + '" '
          const html = doc.convert()
          expect(html).to.equal(`<div class="sect1">
<h2 id="_this_is_a_section_before">${sectnumf('1. ')}This is a section before</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
<div class="sect2">
<h3 id="_this_is_a_subsection_before">${sectnumf('1.1. ')}This is a subsection before</h3>
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_page_section_adoc_this_is_a_section">${sectnumf('2. ')}This is a section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_4_5_component_a_module_a_page_page_section2_adoc_this_is_a_section_2">${sectnumf('3. ')}This is a section 2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section two.</p>
</div>
<div class="sect2">
<h3 id="_4_5_component_a_module_a_page_page_section3_adoc_this_is_a_section_3">${sectnumf('3.1. ')}This is a section 3</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence of section three.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_this_is_a_section_after">${sectnumf('4. ')}This is a section after.</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a short paragraph.</p>
</div>
</div>
</div>`)
        })
      })
    })
  })
})

function structure (node, level = 0, correctLevel = 0) {
  // if (node.context === 'document' || node.context === 'section') {
  //   correctLevel = node[ainclude.$correctLevel] || correctLevel
  //   console.log(`${'*'.repeat(level)} $$ID: ${node.$$id},`
  //   + `type: ${node.context}, node.level: ${node.level}, correctLevel: ${correctLevel}`)
  //   node.blocks.forEach((block) => structure(block, level + 1, correctLevel) )
  // }
}
