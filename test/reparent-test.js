/* eslint-env mocha */
'use strict'

const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}

const { expect } = require('chai')
const reparent = require('../lib/reparent').reparent

const DEBUG = false

const doc = (level, contentContext, ...blocks) => {
  const doc = { context: 'document', contentContext, level, blocks }
  doc.document = doc
  return doc
}
const sec = (level, ...blocks) => {
  return { context: 'section', level, blocks }
}
const par = (level, ...blocks) => {
  return { context: 'paragraph', level, blocks }
}

const docPlain = (level, ...blocks) => doc(level, false, ...blocks)
const docWpar = (level, ...blocks) => doc(level, false, par(level + 1), ...blocks)
const docWsec = (level, ...blocks) => doc(level, false, sec(level + 1), ...blocks)
const docWsecWpar = (level, ...blocks) => doc(level, false, secWpar(level), ...blocks)

const secWpar = (level, ...blocks) => sec(level, par(level), ...blocks)

//This one can result in some unlikely structures, but the alternative is not a reasonable document structure.
const innerdocWpar = (level, ...blocks) => doc(level - 1, 'paragaph', par(level - 1, ...blocks))
const innerdocWsec = (level, ...blocks) => doc(level, 'section', sec(level, ...blocks))
const innerdocWsecWpar = (level, ...blocks) => doc(level, 'section', secWpar(level, ...blocks))

const label = (node, parent = null, id = 0) => {
  node.$$id = ++id
  parent && (node.parent = parent)
  node.blocks.forEach((child) => {
    id = label(child, node, id)
  })
  return id
}

const checkInOrder = (node, status) => {
  status.index++
  if (node.$$id !== status.index) {
    console.log(`node ${node.$$id} out of order, expected ${status.index}`)
    status.valid = false
  }
  node.blocks.forEach((child) => checkInOrder(child, status))
}

const replacer = (key, value) => key === 'parent' ? value.$$id : key === 'document' ? value.$$id : value

describe('reparent tests', () => {
  function test (test, rp) {
    const testCount = label(test)
    DEBUG && console.log('original', toString(test))
    const rpCount = label(rp)
    expect(rpCount).to.equal(testCount)
    const visited = reparent(test)
    DEBUG && console.log('reparented', toString(test))
    expect(visited).to.equal(rpCount)
    const status = { valid: true, index: 0 }
    checkInOrder(test, status)
    expect(status.valid).to.equal(true)
    expect(status.index).to.equal(visited)
    expect(toString(test)).to.equal(toString(rp))
    expect(test).to.deep.equal(rp)
  }

  ;[
    { docType: 'plain', docf: docPlain },
    { docType: 'docWpar', docf: docWpar },
    { docType: 'docWsec', docf: docWsec },
    { docType: 'docWsecWpar', docf: docWsecWpar },
  ].forEach(({ docType, docf }) => {
    ;[
      { sectionType: 'plain', secf: sec },
      { sectionType: 'with par', secf: secWpar },
    ].forEach(({ sectionType, secf }) => {
      ;[
        { innerDocType: 'innerdocWpar', innerdocf: innerdocWpar },
        { innerDocType: 'innerdocWsec', innerdocf: innerdocWsec },
        { innerDocType: 'innerdocWsecWpar', innerdocf: innerdocWsecWpar },
      ].forEach(({ innerDocType, innerdocf }) => {
        it(`one, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(1),
                innerdocf(1)),
              secf(1)
            ),
            docf(0,
              secf(1),
              innerdocf(1),
              innerdocf(1),
              secf(1)
            )
          )
        })

        it(`two, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2),
                innerdocf(1)),
              secf(1)
            ),
            docf(0,
              secf(1,
                innerdocf(2)),
              innerdocf(1),
              secf(1)
            )
          )
        })

        it(`three, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(1),
                innerdocf(2)),
              secf(1)
            ),
            docf(0,
              secf(1),
              innerdocf(1,
                innerdocf(2)),
              secf(1)
            )
          )
        })

        it(`four, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2),
                innerdocf(2)),
              secf(1)
            ),
            docf(0,
              secf(1,
                innerdocf(2),
                innerdocf(2)),
              secf(1)
            )
          )
        })

        it(`five, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2),
                innerdocf(3)),
              secf(1)
            ),
            docf(0,
              secf(1,
                innerdocf(2,
                  innerdocf(3))),
              secf(1)
            )
          )
        })

        it(`six, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(1),
                innerdocf(1)),
              secf(2)
            ),
            docf(0,
              secf(1),
              innerdocf(1),
              innerdocf(1,
                secf(2))
            )
          )
        })

        it(`seven, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2),
                innerdocf(3)),
              secf(4)
            ),
            docf(0,
              secf(1,
                innerdocf(2,
                  innerdocf(3,
                    secf(4))))
            )
          )
        })

        it(`eight, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2,
                  secf(3))),
              secf(3)
            ),
            docf(0,
              secf(1,
                innerdocf(2,
                  secf(3),
                  secf(3)))
            )
          )
        })

        it(`nine, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2,
                  secf(3,
                    secf(4)))),
              secf(3)
            ),
            docf(0,
              secf(1,
                innerdocf(2,
                  secf(3,
                    secf(4)),
                  secf(3)))
            )
          )
        })

        //nested
        it(`ten, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(1,
                  innerdocf(1))),
              secf(1)
            ),
            docf(0,
              secf(1),
              innerdocf(1),
              innerdocf(1),
              secf(1)
            )
          )
        })

        it(`eleven, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2,
                  innerdocf(1))),
              secf(1)
            ),
            docf(0,
              secf(1,
                innerdocf(2)),
              innerdocf(1),
              secf(1)
            )
          )
        })

        it(`twelve, docType: ${docType}, sectionType: ${sectionType}, innerDocType: ${innerDocType}`, () => {
          test(
            docf(0,
              secf(1,
                innerdocf(2,
                  innerdocf(2))),
              secf(1)
            ),
            docf(0,
              secf(1,
                innerdocf(2),
                innerdocf(2)),
              secf(1)
            )
          )
        })
      })
    })
  })
})

function toString (test) {
  const asString = JSON.stringify(test, replacer, 2)
  return asString
}

describe('construction tests', () => {
  it('one', () => {
    const test = docPlain(0,
      sec(1,
        par(2),
        innerdocWsec(1),
        innerdocWsec(1),
        sec(2))
    )
    const count = label(test)
    const asString = toString(test)
    expect(count).to.equal(8)
    expect(asString).to.equal(`{
  "context": "document",
  "contentContext": false,
  "level": 0,
  "blocks": [
    {
      "context": "section",
      "level": 1,
      "blocks": [
        {
          "context": "paragraph",
          "level": 2,
          "blocks": [],
          "$$id": 3,
          "parent": 2
        },
        {
          "context": "document",
          "contentContext": "section",
          "level": 1,
          "blocks": [
            {
              "context": "section",
              "level": 1,
              "blocks": [],
              "$$id": 5,
              "parent": 4
            }
          ],
          "document": 4,
          "$$id": 4,
          "parent": 2
        },
        {
          "context": "document",
          "contentContext": "section",
          "level": 1,
          "blocks": [
            {
              "context": "section",
              "level": 1,
              "blocks": [],
              "$$id": 7,
              "parent": 6
            }
          ],
          "document": 6,
          "$$id": 6,
          "parent": 2
        },
        {
          "context": "section",
          "level": 2,
          "blocks": [],
          "$$id": 8,
          "parent": 2
        }
      ],
      "$$id": 2,
      "parent": 1
    }
  ],
  "document": 1,
  "$$id": 1
}`)
  })

  it('secWpar', () => {
    const test = docPlain(0,
      secWpar(1,
        innerdocWsec(1),
        innerdocWsec(1),
        sec(2))
    )
    const count = label(test)
    const asString = toString(test)
    expect(count).to.equal(8)
    expect(asString).to.equal(`{
  "context": "document",
  "contentContext": false,
  "level": 0,
  "blocks": [
    {
      "context": "section",
      "level": 1,
      "blocks": [
        {
          "context": "paragraph",
          "level": 1,
          "blocks": [],
          "$$id": 3,
          "parent": 2
        },
        {
          "context": "document",
          "contentContext": "section",
          "level": 1,
          "blocks": [
            {
              "context": "section",
              "level": 1,
              "blocks": [],
              "$$id": 5,
              "parent": 4
            }
          ],
          "document": 4,
          "$$id": 4,
          "parent": 2
        },
        {
          "context": "document",
          "contentContext": "section",
          "level": 1,
          "blocks": [
            {
              "context": "section",
              "level": 1,
              "blocks": [],
              "$$id": 7,
              "parent": 6
            }
          ],
          "document": 6,
          "$$id": 6,
          "parent": 2
        },
        {
          "context": "section",
          "level": 2,
          "blocks": [],
          "$$id": 8,
          "parent": 2
        }
      ],
      "$$id": 2,
      "parent": 1
    }
  ],
  "document": 1,
  "$$id": 1
}`)
  })
})
