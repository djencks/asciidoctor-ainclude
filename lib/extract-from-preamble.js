'use strict'

//TODO consider if all blocks after one to be lifted need to be lifted, to preserver ordering.
function extractFromPreamble (doc) {
  if (doc.blocks.length && doc.blocks[0].context === 'preamble') {
    const preamble = doc.blocks[0]
    let targetIndex = 1
    let block
    for (let preambleIndex = 0; preambleIndex < preamble.blocks.length;) {
      if (isSection(block = preamble.blocks[preambleIndex])) {
        doc.blocks.splice(targetIndex++, 0, block)
        preamble.blocks.splice(preambleIndex, 1)
        block.parent = doc
      } else {
        preambleIndex++
      }
    }
    //remove preamble if empty
    if (preamble.blocks.length === 0) {
      doc.blocks.shift()
    }
  }

  function isSection (block) {
    return block.context === 'section' || block.contentContext === 'section'
  }
}

module.exports.extractFromPreamble = extractFromPreamble
