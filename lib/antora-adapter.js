'use strict'

const Opal = global.Opal

const ospath = require('path')
const { posix: path } = ospath

const AincludeVfsContext = require('./vfs-context')
const DEBUG = false

//characters to replace by '_' in generated idprefix
const IDRX = /[/ _.-]+/g
module.exports.IDRX = IDRX

const getKey = (target) => {
  return `${target.src.version}@${target.src.component}:${target.src.module}:${target.src.family}$${target.src.relative}`
}

module.exports.get = (config) => {
  const { contentCatalog } = config
  const vfsContext = (config.file && config.file.vfsContext) ||
      new AincludeVfsContext(getKey(config.file), config.file)
  config.file && !config.file.vfsContext && (config.file.vfsContext = vfsContext)
  return {
    read: function (resourceId) {
      const target = contentCatalog.resolveResource(resourceId, vfsContext.current().file.src)
      if (!target) {
        console.warn(`Could not locate ${resourceId} in context of ${vfsContext.current().token}`)
        return
      }
      DEBUG && console.log(`located ${resourceId} in context of ${vfsContext.current().token}`)
      const token = getKey(target)
      const current = vfsContext.push(token, target)
      const idprefix = `_${target.src.version}_${target.src.component}_${target.src.module}_${target.src.family}_${target.src.relative}`
        .replace(IDRX, '_')
      const attributeOverrides = overrides(target, contentCatalog)
      return {
        content: (target.src.contents && target.src.contents.toString()) || target.contents.toString(),
        current,
        targetInfo: { idprefix, token },
        attributeOverrides,
      }
    },
    pop: function () {
      vfsContext.pop()
    },
  }
}

// This code is derived from the Antora asciidoc-loader.

function overrides (file, contentCatalog) {
  const { family, relative, extname = path.extname(relative) } = file.src
  const intrinsicAttrs = {
    docname: (family === 'nav' ? 'nav$' : '') + relative.substr(0, relative.length - extname.length),
    docfile: file.path,
    // NOTE docdir implicitly sets base_dir on document; Opal only expands value to absolute path if it starts with ./
    docdir: file.dirname,
    docfilesuffix: extname,
    // NOTE relfilesuffix must be set for page-to-page xrefs to work correctly
    // Here, it prevents xrefs from working properly.
    // relfilesuffix: '.adoc',
    imagesdir: path.join(file.pub.moduleRootPath, '_images'),
    attachmentsdir: path.join(file.pub.moduleRootPath, '_attachments'),
    // examplesdir: EXAMPLES_DIR_TOKEN,
    // partialsdir: PARTIALS_DIR_TOKEN,
  }
  const attributes = Object.assign({}, intrinsicAttrs, computePageAttrs(file.src, contentCatalog))
  return Opal.hash2(Object.keys(attributes), attributes)
}

// QUESTION should we soft set the page-id attribute?
function computePageAttrs ({ component: componentName, version, module: module_, relative, origin }, contentCatalog) {
  const attrs = {}
  attrs['page-component-name'] = componentName
  attrs['page-component-version'] = attrs['page-version'] = version
  const component = contentCatalog && contentCatalog.getComponent(componentName)
  if (component) {
    const componentVersion = component.versions.find((it) => it.version === version)
    if (componentVersion) attrs['page-component-display-version'] = componentVersion.displayVersion
    attrs['page-component-title'] = component.title
  }
  attrs['page-module'] = module_
  attrs['page-relative'] = attrs['page-relative-src-path'] = relative
  if (origin) {
    attrs['page-origin-type'] = origin.type
    attrs['page-origin-url'] = origin.url
    attrs['page-origin-start-path'] = origin.startPath
    if (origin.branch) {
      attrs['page-origin-refname'] = attrs['page-origin-branch'] = origin.branch
      attrs['page-origin-reftype'] = 'branch'
    } else {
      attrs['page-origin-refname'] = attrs['page-origin-tag'] = origin.tag
      attrs['page-origin-reftype'] = 'tag'
    }
    if (origin.worktree) {
      attrs['page-origin-worktree'] = ''
      attrs['page-origin-refhash'] = '(worktree)'
    } else {
      attrs['page-origin-refhash'] = origin.refhash
    }
  }
  return attrs
}
