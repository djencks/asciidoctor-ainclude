'use strict'

const DEBUG = false
const Opal = global.Opal

const antoraAdapter = require('./antora-adapter')
const extractFromPreamble = require('./extract-from-preamble').extractFromPreamble
const reparent = require('./reparent').reparent
const reindexSections = require('./reindex-sections').reindexSections

const empty = () => Opal.hash2([], {})

const $precedingAttributes = Symbol('precedingAttributes')
const $fileContext = Symbol('fileContext')

//Allows access to the resource id of the included pages, and the idprefix used for them.
//TODO can the asciidoctor catalog be used for this?
const included = {}

module.exports.included = included
module.exports.$fileContext = $fileContext

module.exports.register = function (registry, config = {}) {
  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  //For use by antora-pdf, file may have a 'vfsContext' field.

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.
  //File access
  const vfs = (config.vfs && typeof config.vfs.read === 'function')
    ? config.vfs.included = included && config.vfs
    //Antora support
    : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
      //Antora support
      ? antoraAdapter.get(config)
      //look in the file system
      : fsAccess()

  function fsAccess () {
    const fs = require('fs')
    return {
      read: (path, context = undefined, encoding = 'utf8') => {
        if (path.startsWith('file://')) {
          return {
            content: fs.readFileSync(path.substr('file://'.length), encoding).toString(),
            current: { token: path, file: null },
            targetInfo: { targetAttributes: {}, included: path },
            attributeOverrides: empty(),
          }
        }
        return {
          content: fs.readFileSync(path, encoding).toString(),
          targetAttributes: {},
        }
      },
      pop: () => {
      },
    }
  }

  const Document = Opal.module(null, 'Asciidoctor').Document
  const PreprocessorReader = Opal.module(null, 'Asciidoctor').PreprocessorReader
  const Cursor = Opal.module(null, 'Asciidoctor').Reader.Cursor

  // var self = Opal.top
  // var $nesting = []
  var nil = Opal.nil
  // var $$$ = Opal.const_get_qualified
  // var $$ = Opal.const_get_relative
  // var $breaker = Opal.breaker
  // var $slice = Opal.slice
  // var $module = Opal.module
  // var $klass = Opal.klass
  // var $send = Opal.send
  var $truthy = Opal.truthy
  var $hash2 = Opal.hash2
  // var $gvars = Opal.gvars
  // var $hash = Opal.hash

  const innerDocument = (() => {
    const scope = Opal.klass(
      Opal.module(null, 'Ainclude', function () {
      }),
      Document,
      'innerDocument',
      function () {
      }
    )
    Opal.defn(scope, '$initialize', function initialize (data, options, precedingAttributes, headerAttributes, initialParent) {
      this[$precedingAttributes] = precedingAttributes
      //We start by just attaching to the section/doc context and use a treeprocessor to reorder later.
      this.parent = initialParent
      //parse nothing with reader
      Opal.send(this, Opal.find_super_dispatcher(this, 'initialize', initialize), ['', options])
      this.attributes = this.attributes.$merge(headerAttributes)
      //Act like a standalone doc:
      //switch to a preprocessor reader set to actual input.
      const self = this
      self.parsed = Opal.nil
      self.reader = PreprocessorReader.$new(self, data, Cursor.$new(self.attributes['$[]']('docfile'), self.base_dir), $hash2(['normalize'], { normalize: true }))
      if ($truthy(self.sourcemap)) {
        return (self.source_location = self.reader.$cursor())
      } else {
        return nil
      }
    })

    Opal.defn(scope, '$convert', function $$convert (opts) {
      var self = this
      self.parent_document.$playback_attributes(self[$precedingAttributes])
      return Opal.send(this, Opal.find_super_dispatcher(this, 'convert', $$convert), [opts])
    })

    return scope
  })()

  const attributeEntry = Document.AttributeEntry

  //This is used only for processing the listToAinclude files
  const Html5Converter = (() => {
    const scope = Opal.klass(
      Opal.module(null, 'Ainclude', {}),
      Opal.module(null, 'Asciidoctor').Converter.Html5Converter,
      'Html5ListConverter',
      function () {
      }
    )

    const $xrefCallback = Symbol('xrefCallback')

    Opal.defn(scope, '$initialize', function initialize (backend, opts, xrefCallback) {
      Opal.send(this, Opal.find_super_dispatcher(this, 'initialize', initialize), [backend, opts])
      this[$xrefCallback] = xrefCallback
    })

    Opal.defn(scope, '$inline_anchor', function convertInlineAnchor (node) {
      if (node.getType() === 'xref') {
        let callback
        const refSpec = node.getAttribute('path', undefined, false)
        if (refSpec && (callback = this[$xrefCallback])) {
          DEBUG && console.log('refspec: ', refSpec)
          callback(refSpec)
        }
      }
      return Opal.send(this, Opal.find_super_dispatcher(this, 'inline_anchor', convertInlineAnchor), [node])
    })
    Opal.defn(scope, '$convert_inline_anchor', function convertInlineAnchor (node) {
      if (node.getType() === 'xref') {
        let callback
        const refid = node.getAttribute('refid', undefined, false)
        if (refid && (callback = this[$xrefCallback])) {
          callback(refid)
        }
      }
      return Opal.send(this, Opal.find_super_dispatcher(this, 'convert_inline_anchor', convertInlineAnchor), [node])
    })
    return scope
  })()

  const ATTRIBUTE_OVERRIDES = 'attribute_overrides'
  const HEADER_ATTRIBUTES = 'header_attributes'
  const ALL_ATTRIBUTES = 'all_attributes'
  const ORIGINAL_IDS = 'original_ids'
  const UNIQUE_IDS = 'unique_ids'

  //Attributes that are always transferred, even with ATTRIBUTE_OVERRIDES set.
  const WHITELIST = ['sectnums', 'sectnumlevels', 'docdate']

  function toOpts (optsString) {
    const optsArray = (optsString || '').split(',')
    const result = {}
    const attributeVisibility = optsArray.includes(ATTRIBUTE_OVERRIDES)
      ? ATTRIBUTE_OVERRIDES
      : optsArray.includes(HEADER_ATTRIBUTES)
        ? HEADER_ATTRIBUTES
        : optsArray.includes(ALL_ATTRIBUTES)
          ? ALL_ATTRIBUTES : undefined
    if (attributeVisibility) result.attributeVisibility = attributeVisibility
    if (optsArray.includes(ORIGINAL_IDS)) {
      result.uniqueIds = ORIGINAL_IDS
    } else if (optsArray.includes(UNIQUE_IDS)) {
      result.uniqueIds = UNIQUE_IDS
    }
    return result
  }

  function aincludeBlockMacro () {
    const self = this
    self.named('ainclude')
    self.positionalAttributes(['leveloffset', '!opts'])
    self.process(ainclude)
  }

  function ainclude (parent, target, attributes) {
    const opts = Object.assign({ attributeVisibility: ATTRIBUTE_OVERRIDES, uniqueIds: UNIQUE_IDS },
      toOpts(parent.document.header_attributes.$$smap['ainclude-default-options']),
      toOpts(attributes['!opts']))
    delete attributes['!opts']
    const { content, targetInfo, attributeOverrides } = vfs.read(target)
    const idprefix = (targetInfo && opts.uniqueIds === UNIQUE_IDS) ? targetInfo.idprefix : ''
    const precedingAttributeEntries = attributes.attribute_entries
    delete attributes.attribute_entries
    const precedingAttributes = Opal.hash2(['attribute_entries'], { attribute_entries: precedingAttributeEntries })

    parent.document.$playback_attributes(precedingAttributes)
    //Save attributes up to here, including preceding attributes; used to reset parent attr at end.
    const originalAttributes = parent.document.$attributes()
    if (attributes.leveloffset) {
      const oldleveloffset = parseInt(originalAttributes.$$smap.leveloffset || '0')
      if (attributes.leveloffset[0] === '+') {
        attributes.leveloffset = '' + (oldleveloffset + parseInt(attributes.leveloffset.slice(1)))
      } else if (attributes.leveloffset[0] === '-') {
        attributes.leveloffset = '' + (oldleveloffset - parseInt(attributes.leveloffset.slice(1)))
      }
    }
    const leveloffset = parseInt(attributes.leveloffset || originalAttributes.$$smap.leveloffset || '0')
    attributes.fragment = true
    //Turn attributes into a hash suitable for playbackAttributes
    const attrHash = Opal.hash2(['attribute_entries'],
      {
        attribute_entries: Object.entries(Object.assign({ leveloffset }, attributes))
          .map(([name, value]) => {
            return attributeEntry.$new(name, value, false)
          }),
      })
    if (opts.attributeVisibility === ATTRIBUTE_OVERRIDES) {
      const newAttr = parent.document.attributes = empty()
      WHITELIST.forEach((name) => {
        if (originalAttributes['$key?'](name)) {
          newAttr['$[]='](name, originalAttributes['$[]'](name))
        }
      })
    } else if (opts.attributeVisibility === HEADER_ATTRIBUTES) {
      parent.document.attributes = parent.document.header_attributes.$dup()
    } else {
      parent.document.attributes = originalAttributes.$dup()
    }
    //apply attributes for this template
    parent.document.$playback_attributes(attrHash)

    //These will be the initial header attributes for the child.
    const headerAttributes = parent.document.attributes

    //Supply attribute overrides for child document as parent attribute overrides
    //Both $merge and doc initialization make copies for us.
    parent.document.attributes = attributeOverrides.$merge(parent.document.attribute_overrides)

    const options = Opal.hash2(['standalone', 'extension_registry', 'parent'],
      {
        standalone: false,
        extension_registry: registry,
        parent: parent.document,
      })
    DEBUG && console.log(`new innerDoc:opts: , precedingAttributes: ${precedingAttributeEntries}, headerAttributes: ${headerAttributes}`)
    const innerDoc = innerDocument.$new(content, options, precedingAttributes, headerAttributes, parent)
    innerDoc.parse()

    //Adjust leveloffset to actual included section level.
    //Wy only section? because leveloffset otherwise has no effect at all!
    if (innerDoc.blocks[0].context === 'section') {
      innerDoc.level = innerDoc.blocks[0].level
    } else {
      innerDoc.level = leveloffset //it seems to get reset somehow
      innerDoc.blocks.forEach((block) => { block.level = leveloffset })
    }
    innerDoc.contentContext = innerDoc.blocks[0].contentContext || innerDoc.blocks[0].context

    //compute the id and idprefix for the document, and make them available on the innerDoc.
    if (innerDoc.blocks.length) {
      if (idprefix && (!innerDoc.blocks[0].id || innerDoc.blocks[0].id === Opal.nil)) {
        innerDoc.blocks[0].id = idprefix
      }
      if (targetInfo.token) {
        included[targetInfo.token] = {
          idprefix,
          innerDoc,
        }
        innerDoc[$fileContext] = targetInfo.token
      }
    }
    vfs.pop()
    parent.document.attributes = originalAttributes
    return innerDoc
  }

  function adjustIdsTreeProcessor () {
    const self = this
    self.process(function (doc) {
      const refs = doc.catalog.$$smap.refs
      traverse(doc, included[doc[$fileContext]])
      return doc

      function traverse (node, context) {
        if (context && node.id && node.id !== Opal.nil) {
          if (node.id !== context.idprefix) {
            refs.$delete(node.id)
            node.id = context.idprefix + node.id
            refs['$[]='](node.id, node)
          }
        } else if (node[$fileContext]) {
          context = included[node[$fileContext]]
        }
        // node.blocks || console.log('node without blocks', node)
        if (node.blocks) {
          node.blocks.forEach((block) => {
            traverse(block, context)
          })
        } else if (Array.isArray(node)) {
          [].concat(...node).forEach((listitem) => {
            traverse(listitem, context)
          })
        }
        if (node[$fileContext]) {
          context.docid = node.blocks[0].id
        }
      }
    })
  }

  function correctLevelsTreeProcessor () {
    const self = this
    self.process(function (doc) {
      DEBUG && console.log('before')
      DEBUG && structure(doc)
      extractFromPreamble(doc)
      reparent(doc)
      reindexSections(doc)
      DEBUG && console.log('after')
      DEBUG && structure(doc)
      return doc
    })
  }

  function blockInfo (node) {
    return `$$ID: ${node.$$id}, type: ${node.context}, node.level: ${node.level}, numeral: ${node.getNumeral()}, title: ${node.title}, document: ${node.document.$$id}`
  }

  function structure (node, level = 0) {
    if (node.blocks) {
      console.log(`${'*'.repeat(level)} ${blockInfo(node)}`)
      node.blocks.forEach((block) => structure(block, level + 1))
    } else {
      console.log(`not a node: ${typeof node}`)
    }
  }

  /* Include a document that is expected to be one or more possibly nested lists of xrefs to content,
   * such as an Antora nav file. The xrefs are converted to aincludes and the resulting subdocuments appended to the
   * parent blocks.
   */
  function listToAincludeBlockMacro () {
    const self = this
    self.named('listToAinclude')
    self.positionalAttributes(['leveloffset', '!opts'])
    self.process(function (parent, target, attributes) {
      //some global variables for traverse
      const numbered = parent.document.hasAttribute('sectnums')
      var innerDocFlag = false

      const originalAttributes = parent.document.$attributes().$merge(empty())
      var leveloffset = 0
      if (attributes.leveloffset) {
        const oldleveloffset = parseInt(originalAttributes.$$smap.leveloffset || '0')
        if (attributes.leveloffset[0] === '+') {
          leveloffset = oldleveloffset + parseInt(attributes.leveloffset.slice(1))
          attributes.leveloffset = '' + leveloffset
        } else if (attributes.leveloffset[0] === '-') {
          leveloffset = oldleveloffset - parseInt(attributes.leveloffset.slice(1))
          attributes.leveloffset = '' + leveloffset
        } else {
          leveloffset = parseInt(attributes.leveloffset)
        }
      }
      // read the 'nav' file and pop it
      const { content, current } = vfs.read(target)

      const converter = Html5Converter.$new('html5', undefined, processXref)

      //Add extensions provided in config, using original config as config
      const extensionRegistry = Opal.module(null, 'Asciidoctor').Extensions.create()
      const extensions = (config.config && config.config.aincludeExtensions) || []

      const newConfig = Object.assign({}, config, { file: current.file })
      if (extensions.length) extensions.forEach((ext) => ext.register(extensionRegistry, newConfig))

      const options = Opal.hash2(['attributes', 'doctype', 'standalone', 'extension_registry', 'converter', 'safe'],
        {
          attributes: originalAttributes,
          doctype: 'inline',
          standalone: false,
          extension_registry: extensionRegistry,
          converter,
          safe: 'safe',
          // parent: parent.document,
        })
      const listDoc = Document.$new(content, options).parse()
      traverse(parent, listDoc, leveloffset + 1)
      vfs.pop()

      //from inline template
      function traverse (parent, node, lo) {
        node.blocks
          .filter((block) => block.context === 'ulist')
          .forEach((ulist) =>
            ulist.blocks
              .filter((block) => block.context === 'list_item')
              .forEach((listItem) => {
                leveloffset = lo
                innerDocFlag = false
                const listItemText = listItem.$text()
                DEBUG && console.log(`listItem.text: ${listItemText}, leveloffset: ${lo}, numbered: ${numbered}, innerDocFlag: ${innerDocFlag}`)
                if (!innerDocFlag) {
                  const section = self.$create_section(parent, listItemText, Opal.hash2([], {}), Opal.hash2(['level'], { level: leveloffset }))
                  parent.blocks.push(section)
                }
                traverse(parent, listItem, lo + 1)
              }))
      }

      function processXref (refid) {
        DEBUG && console.log(`processXref, refid: ${refid}, leveloffset: ${leveloffset}`)
        const innerDoc = ainclude(parent, refid, { leveloffset })
        innerDocFlag = true
        innerDoc && parent.blocks.push(innerDoc)
      }
    })
  }

  function doRegister (registry) {
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(aincludeBlockMacro)
      registry.blockMacro(listToAincludeBlockMacro)
    } else {
      console.warn('no \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.treeProcessor === 'function') {
      registry.treeProcessor(adjustIdsTreeProcessor)
      registry.treeProcessor(correctLevelsTreeProcessor)
    } else {
      console.warn('no \'treeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the actual registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}
