'use strict'

class vfsContext {
  constructor (token, file) {
    this.contexts = []
    this.push(token, file)
  }

  push (token, file) {
    this.contexts.unshift({ token, file })
    return this.current()
  }

  pop () { return this.contexts.shift() }

  current () { return this.contexts[0] }
}

module.exports = vfsContext
