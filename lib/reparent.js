'use strict'

const Opal = global.Opal

const DEBUG = false

function reparent (node, startIndex = 0) {
  DEBUG && console.log(`visited node ${node.$$id}, level: ${node.level}`)
  var visited = 1
  for (var i = startIndex; i < node.blocks.length; i++) {
    const child = node.blocks[i]
    if (child.context === 'document' || child.context === 'section') { //TODO check for $precedingAttributes as well
      const amount = compareLevels(node, child)
      DEBUG && console.log(`node: ${node.$$id}, index (i): ${i}, child: ${child.$$id}, child context: ${child.context}, child contentContext: ${child.contentContext}, amount: ${amount}, node.blocks:`, node.blocks.map((c) => c.$$id))
      if (amount < 0) {
        const { newParent, index } = up(node, amount)
        const toMove = node.blocks.splice(i, node.blocks.length) //be sure we get them all!
        moveTo(newParent, toMove, index)
        DEBUG && console.log(`UP newParent: ${newParent.$$id}, newParent.blocks:`, newParent.blocks.map((c) => c.$$id))
        //we are done with this node's blocks; all previous blocks have been processed,
        //and we just moved all the remaining blocks up.
        //It is not possible for more nodes to be added to this node's blocks.
      } else if (amount === 0) {
        visited += reparent(child)
        if (child.context === 'document') {
          child.document = child
        }
      } else if (i === 0) {
        //there's nothing to move down against.  Or can we look higher?? No, that would change the block order.
        console.log(`first block ${child.$$id} cannot be moved down, leaving in place.`)
        visited += reparent(child)
      } else {
        const toMove = node.blocks.splice(i, 1) //only one for down.
        //We just removed the block we are iterating through, if one is added later be sure to traverse it.
        i--
        const { newParent, index } = down(node, amount, i)
        if (newParent === node || newParent.level >= child.level) {
          //we can't move any farther
          console.log(`cannot move ${child.$$id} ${child.context} down farther from ${node.$$id} ${node.context} ${node.title}`)
          // put it back
          node.blocks.splice(i + 1, 0, ...toMove)
          i++
          visited += reparent(child)
        } else {
          moveTo(newParent, toMove, index)
          DEBUG && console.log(`DOWN newParent: ${newParent.$$id}, newParent.blocks:`, newParent.blocks.map((c) => c.$$id))
          // we are going to revisit this block, so don't count the vist.
          visited += reparent(newParent, index + 1) - 1
        }
      }
    } else if (child.context === 'dlist') {
      //Do nothing for now
    } else {
      DEBUG && console.log(`not moving child of node: ${node.$$id}, index (i): ${i}, context: ${node.context}, child: ${child.$$id}, child context: ${child.context}, child contentContext: ${child.contentContext}, node.blocks:`, node.blocks.map((c) => c.$$id))
      visited += reparent(child)
    }
  }
  DEBUG && console.log(`leaving node: ${node.$$id}, node.blocks:`, node.blocks.map((c) => c.$$id))
  return visited

  function compareLevels (parent, child) {
    if (parent.context === 'document' && parent.level === child.level) return 0 //?? also && child.context === 'section'????
    if ((parent.context === 'section' || (parent.context === 'document' && parent.level === 0)) &&
      (child.contentContext || child.context) !== 'section') return child.level - parent.level
    return child.level - parent.level - 1
  }

  //todo turn left recursion into iteration
  // distance is negative number of levels to move up.
  //'node' is 'oldParent'
  function up (node, distance) {
    function top (n) {
      return !n.parent || n.parent === Opal.nil
    }

    if (top(node)) {
      console.log(`Reached the top at ${node.$$id} with ${distance} remaining`)
      return { newParent: node, index: node.blocks.length - 1 } //??? -1?
    }
    if (node.level === node.parent.level && !top(node.parent)) { //?? && node.parent.context === 'document'
      node = node.parent
    }
    if (distance === -1) {
      return { newParent: node.parent, index: node.parent.blocks.indexOf(node) }
    }
    return up(node.parent, distance + 1)
  }

  //todo turn left recursion into iteration
  // distance is number of levels to move down.
  //'node' is 'oldParent'
  function down (node, distance, startIndex) {
    if (node.blocks.length <= startIndex) {
      console.log(`Reached the bottom at ${node.$$id} with ${distance} remaining`)
      return { newParent: node, index: node.blocks.length - 1 } //??? -1?
    }
    var newParent = node.blocks[startIndex]
    if (newParent.blocks.length && newParent.level === lastChild(newParent).level) { //?? && node.context === 'document'
      newParent = lastChild(newParent)
    }
    if (distance === 1) {
      return { newParent, index: newParent.blocks.length - 1 }
    }
    return down(newParent, distance - 1, newParent.blocks.length - 1)
  }

  function lastChild (node) { return node.blocks[node.blocks.length - 1] }

  function moveTo (newParent, toMove, index) {
    toMove.forEach((node) => {
      node.parent = newParent
      if (node.context === 'document') {
        node.document = node
      }
    })
    newParent.blocks.splice(index + 1, 0, ...toMove)
  }
}

module.exports.reparent = reparent
