'use strict'

const DEBUG = false

/* ruby implementation in abstract_block.rb
  def reindex_sections
    @next_section_index = 0
    @next_section_ordinal = 1
    @blocks.each do |block|
      if block.context == :section
        assign_numeral block
        block.reindex_sections
      end
    end
  end
 */

function reindexSections (block) {
  block.next_section_index = 0
  block.next_section_ordinal = 1
  block.blocks.forEach((child) => {
    reindexChild(block, block, child)
  })
}

function reindexChild (section, block, child) {
  if (child.context === 'document') {
    child.next_section_index = block.next_section_index
    child.next_section_ordinal = block.next_section_ordinal
    child.blocks.forEach((grandChild) => {
      reindexChild(section, child, grandChild)
    })
    block.next_section_index = child.next_section_index
    block.next_section_ordinal = child.next_section_ordinal
  } else if (child.context === 'section') {
    block.$assign_numeral(child)
    child.parent = section
    DEBUG && console.log(`section: ${child.$$id}, sectnum: ${child.$sectnum('.', '.')}`)
    reindexSections(child)
  }
}

module.exports.reindexSections = reindexSections
