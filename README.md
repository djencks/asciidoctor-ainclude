# Asciidoctor ainclude Extension
:version: 0.0.3

`@djencks/asciidoctor-ainclude` provides an Asciidoctor.js extension to include subdocuments.
This differs from the include preprocessor instruction in that only syntactically correct subdocuments may be include, attributes are isolated, and ids are transformed to be unique.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-ainclude/-/blob/master/README.adoc.

## Installation

Available soon through npm as @djencks/asciidoctor-ainclude.

Currently available through a line like this in `package.json`:


    "@djencks/asciidoctor-ainclude": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-asciidoctor-ainclude-v0.0.1.tgz",


The project git repository is https://gitlab.com/djencks/asciidoctor-ainclude

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-ainclude/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-ainclude/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension is under extensions/ainclude-extension in `https://gitlab.com/djencks/simple-examples`.
